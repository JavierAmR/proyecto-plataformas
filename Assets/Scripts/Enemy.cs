﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [Header("Enemy Settings")]
    public int health;
    public int damage;
    public Vector2 knockbackForce;

    PlayerController mainCharacter;

    private void Awake()
    {
        mainCharacter = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PlayerAttack") 
        {
            health -= mainCharacter.damage;
            if (health <= 0) 
            {
                Destroy(gameObject);
            }
        }
    }

}
