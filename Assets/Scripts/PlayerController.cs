﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    Rigidbody2D rb2d = null;
    public Collider2D attackHitbox;

    UIController uimanager;

    bool jump;
    bool isDead = false;
    protected bool isGrounded = true;
    bool isAttacking = false;

    Vector3 scaleChange;
    Vector2 knockbackDirection;

    Animator animationController;

    [Header("Stats")]
    public int health;
    public float damagerecoveryTime;
    bool invincible = false;
    float tmpRecovery = 0;


    [Header("Attack Settings")]
    public int damage;
    public float prepareTime;
    bool inPreparing;
    public float attackTime;
    bool inAttack;
    public float recoveryTime;
    bool inRecovery;
    float tmp = 0;

    [Header("Movement Settings")]
    public float jumpspeed;
    public float speed;
    //public float maxSpeed;

    private void Awake()
    {
        scaleChange = transform.localScale;
        rb2d = GetComponent<Rigidbody2D>();
        animationController = GetComponentInChildren<Animator>();
        uimanager = GameObject.Find("UI").GetComponent<UIController>();
    }

    
    private void Update()
    {
        if (isDead == false)
        {
            if (isAttacking == true)
            {
                AttackLogic();
            }
            if (invincible == true)
            {
                if (tmpRecovery >= damagerecoveryTime)
                {
                    tmpRecovery = 0;
                    invincible = false;
                }
                tmpRecovery += Time.deltaTime;
            }
            MovementAnimation();
        }
        else 
        {
            Dead();
            SceneManager.LoadScene (3);
        }
    }


    public void Attack()
    {
        if (isGrounded == true && isAttacking == false && isDead == false) 
        {
            isAttacking = true;
            inPreparing = true;
        }
        
    }

    void AttackLogic() 
    {
        if (inPreparing == true)
        {
            
            if (tmp >= prepareTime)
            {
                animationController.SetBool("PreparingAttack", false);
                animationController.SetBool("Attacking", true);
                tmp = 0;
                inPreparing = false;
                inAttack = true;
                attackHitbox.enabled = true;
            }
            else
            {
                animationController.SetBool("PreparingAttack", true);
                tmp += Time.deltaTime;
            }
        }
        if (inAttack == true)
        {
            if (tmp >= attackTime)
            {
                animationController.SetBool("Attacking", false);
                animationController.SetBool("Recovery", true);
                tmp = 0;
                inAttack = false;
                inRecovery = true;
                attackHitbox.enabled = false;
            }
            else
            {
                tmp += Time.deltaTime;
            }
        }
        if (inRecovery == true)
        {
            if (tmp >= prepareTime)
            {
                animationController.SetBool("Recovery", false);
                tmp = 0;
                inRecovery = false;
                isAttacking = false;
            }
            else
            {
                tmp += Time.deltaTime;
            }
        }
    }


    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Ground") 
        {
            isGrounded = false;
        }        
    }


    

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Grounded");
        if (collision.gameObject.tag == "Ground" && isGrounded == false) 
        {
            animationController.SetTrigger("Grounded");
            animationController.SetBool("Falling", false);
            isGrounded = true;
        }
        if (collision.gameObject.tag == "Enemy" && invincible == false)
        {
            invincible = true;
            if (collision.transform.position.x < transform.position.x) 
            {
                rb2d.velocity = Vector2.zero;
                rb2d.AddForce(collision.GetComponent<Enemy>().knockbackForce, ForceMode2D.Impulse);
            }
            else if (collision.transform.position.x > transform.position.x) 
            {
                rb2d.velocity = Vector2.zero;
                rb2d.AddForce(-collision.GetComponent<Enemy>().knockbackForce, ForceMode2D.Impulse);
            }
            if (inAttack == false) 
            {
                Debug.Log(collision.gameObject.GetComponent<Enemy>().damage);
                health -= collision.gameObject.GetComponent<Enemy>().damage;
                uimanager.HealthUpdate();
                if (health <= 0)
                {
                    Debug.Log("Player death");
                    isDead = true;
                }
            }
            
        }
        if (collision.gameObject.tag == "Trap") 
        {
            rb2d.velocity = Vector2.zero;
            rb2d.AddForce(collision.GetComponent<Trap>().knockbackForce, ForceMode2D.Impulse);
            health -= collision.GetComponent<Trap>().damage;
            uimanager.HealthUpdate();

            if (health <= 0)
            {
                
                Debug.Log("Player death");
                isDead = true;

            }
        }
        if (collision.gameObject.tag == "HealthItem") 
        {
            Debug.Log(collision.gameObject.GetComponent<HealthItem>().health);
            health += collision.gameObject.GetComponent<HealthItem>().health;
            uimanager.HealthUpdate();
        }
        if (collision.gameObject.tag == "EnemyAttack" && invincible == false) 
        {
            invincible = true;
            if (inAttack == false)
            {
                Debug.Log(collision.gameObject.GetComponent<EnemyAttack>().damage);
                health -= collision.gameObject.GetComponent<EnemyAttack>().damage;
                uimanager.HealthUpdate();
                if (health <= 0)
                {
                    Debug.Log("Player death");
                    isDead = true;
                }
            }
        }
    }

    public void Jump()
    {
        if (isDead == false)
        {
            if (isGrounded == true)
            {
                rb2d.AddForce(Vector2.up * jumpspeed, ForceMode2D.Impulse);
            }
        }
    }

    public void Movement() 
    {
        if (isDead == false)
        {
            rb2d.AddForce(Vector2.right * Input.GetAxis("Horizontal") * speed);

            if (Input.GetAxis("Horizontal") != 0)
            {
                //animationController.SetBool("Run", true);
                if (Input.GetAxis("Horizontal") < 0 && Mathf.Sign(transform.localScale.x) > 0 || Input.GetAxis("Horizontal") > 0 && Mathf.Sign(transform.localScale.x) < 0)
                {
                    scaleChange.x *= -1;
                    transform.localScale = scaleChange;
                }

            }
            
        }       

    }

    void MovementAnimation() 
    {
        if (rb2d.velocity.x > 0.3 && isGrounded == true || rb2d.velocity.x < -0.1 && isGrounded == true) 
        {
            animationController.SetBool("Run", true);
        }
        else if (rb2d.velocity.x == 0 && Input.GetAxis("Horizontal") == 0)
        {
            animationController.SetBool("Run", false);
        }
        if (rb2d.velocity.y > 0.3 && isGrounded == false) 
        {
            animationController.SetBool("Jump", true);
        }
        if (rb2d.velocity.y < -0.3 && isGrounded == false) 
        {
            animationController.SetBool("Jump", false);
            animationController.SetBool("Falling", true);
        }
    }

    void FixedUpdate()
    {
        Movement();
        /*if (rb2d.velocity.x > maxSpeed)
        {
            rb2d.velocity = new Vector2(maxSpeed, rb2d.velocity.y);
        }
        else if (rb2d.velocity.x < -maxSpeed)
        {
            rb2d.velocity = new Vector2( -maxSpeed, rb2d.velocity.y);
        }*/
    }

    void Dead() 
    {
        Debug.Log("PlayerDeath");
    }
}
