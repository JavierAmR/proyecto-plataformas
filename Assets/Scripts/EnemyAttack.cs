﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    public bool isProjectile;
    public int damage;
    public Vector3 speed;

    private void Update()
    {
        if (isProjectile == true) 
        {
            transform.position += speed*Time.deltaTime;
        }               
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (isProjectile == true) 
        {
            Destroy(gameObject);
        }
        
    }
}
