﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour
{
    Slider healthbar;
    public Slider bossHealthbar;
    PlayerController player;
    public static bool GameIsPaused;
    public GameObject pauseMenuUI;
    [SerializeField] GameObject boss;

    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        healthbar = GameObject.FindGameObjectWithTag("Healthbar").GetComponent<Slider>();
        healthbar.maxValue = player.health;
        bossHealthbar.maxValue = boss.GetComponent<Enemy>().health;
    }

    public void HealthUpdate()
    {
        healthbar.value = player.health;
    }

    public void BossHealthUpdate() 
    {
        bossHealthbar.value = boss.GetComponent<Enemy>().health;
    }

    public void Update() 
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
    }

    void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
    }

    public void QuitGame()
    {
        SceneManager.LoadScene(0);
    }

    public void LoadTestLevel() 
    {
        SceneManager.LoadScene(2);
    }

    public void Exit() 
    {
        SceneManager.LoadScene(0);
    }
}
