﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    
    private Transform cameraTransform;
    private Vector3 lastCameraPosition;

    [SerializeField] private float parallaxEffectMultiplier = 0;

    private void Start()
    {
       cameraTransform = Camera.main.transform;
       lastCameraPosition = cameraTransform.position;
    }

    private void Update()
    {
        Vector3 deltaMovement = cameraTransform.position - lastCameraPosition;
        transform.position += deltaMovement * parallaxEffectMultiplier;
        lastCameraPosition = cameraTransform.position;
    }

   
}
