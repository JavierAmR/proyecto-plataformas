﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{
    Enemy bossStats;
    UIController uimanager;
    private void Awake()
    {
        bossStats = gameObject.GetComponent<Enemy>();
        uimanager = GameObject.FindGameObjectWithTag("UI").GetComponent<UIController>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "PlayerAttack") 
        {
            uimanager.BossHealthUpdate();
        }
    }
}
