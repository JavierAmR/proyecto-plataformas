﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : PlayerController
{
    public AudioClip jumping;
    public AudioClip sword;
    public AudioClip walking;
    private AudioSource source;
    private float volume = 0.6f;
    

    void Awake()
    {
        source = GetComponent<AudioSource>();
    }

    
    void Update()
    {
        if (Input.GetButtonDown("Jump") && isGrounded == true)
        {
            Debug.Log("Sonido salto");
            source.PlayOneShot(jumping, volume);
        }

        if (Input.GetButtonDown("Fire1"))
        {
            Debug.Log("Sonido golpe");
            source.PlayOneShot(sword, volume);
        }


    }
}
