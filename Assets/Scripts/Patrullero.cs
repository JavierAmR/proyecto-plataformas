﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrullero : MonoBehaviour
{
    [SerializeField] Transform[] posiciones;
    [SerializeField] int posactual = 0;
    [SerializeField] float velocidadEnemigo = 1.0f;

    Vector3 scaleChange;

    private void Awake()
    {
        scaleChange = transform.localScale;
    }
    private void Update()
    {
        //Debug.Log("Distancia: " + Vector2.Distance(posiciones[posactual].position, transform.position));
        if (Mathf.Abs(Vector2.Distance(posiciones[posactual].position, transform.position)) < 0.1f)
        {
            posactual++;
            if (posactual >= posiciones.Length)
            {
                posactual = 0;
            }
        }
        if (transform.position.x != posiciones[posactual].position.x) 
        {
            if (transform.position.x < posiciones[posactual].position.x && Mathf.Sign(transform.localScale.x) >= 0 || transform.position.x > posiciones[posactual].position.x && Mathf.Sign(transform.localScale.x) < 0)
            {
                scaleChange.x *= -1;
                transform.localScale = scaleChange;
            }
            
        }

        transform.position = Vector2.MoveTowards(transform.position, posiciones[posactual].position, Time.deltaTime * velocidadEnemigo);
    }
}